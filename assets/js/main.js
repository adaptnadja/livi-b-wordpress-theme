jQuery(document).ready(function() {

	window.sr = ScrollReveal();
	sr.reveal('.s-r');

	jQuery("button.hamburger").click(function()
	{
		if(jQuery(this).hasClass("is-active"))
		{
			jQuery(this).removeClass("is-active");
			jQuery("#navbar-collapse").slideDown();
		}
		else
		{
			jQuery(this).addClass("is-active");
			jQuery("#navbar-collapse").slideUp();
		}
	});
});

setInterval(function()
{
	jQuery("#page-load img").stop().animate(
	{
		'opacity':'0',
	}, 800);

	setTimeout(function()
	{
		jQuery("#page-load img").stop().animate(
		{
			'opacity':'1',
		}, 800);
	}, 810);

}, 1650);

document.onreadystatechange = function()
{
	jQuery('.grid').masonry({
	  // set itemSelector so .grid-sizer is not used in layout
	  itemSelector: '.grid-item',
	  // use element for option
	  columnWidth: '.grid-sizer',
	  percentPosition: true,
	  masonry: {
	    // use outer width of grid-sizer for columnWidth
	    columnWidth: '.grid-sizer'
	  }
	});

	if(document.readyState == 'complete')
	{
		// var portfolioSlider = jQuery('#single').owlCarousel({
	  //   margin:2,
	  //   loop:true,
	  //   items:4,
	  //   autoplay:true,
		// 	nav:true,
		// 	navText:["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		// 	responsive: {
		// 		320 : {
		// 			items:1
		// 		},
		// 		1200: {
		// 			items: 4
		// 		}
		// 	}
		// });

		jQuery(".slick").slick({
			infinite:true,
			autoplay:true,
			slidesToShow:4,
			centerMode:true,
			variableWidth:true,
			speed:300,
			arrows:true,
			prevArrow: '<i class="fa fa-angle-left left"></i>',
			nextArrow: '<i class="fa fa-angle-right right"></i>'
		});

		stickyHeader();
		positionFooter();

		jQuery("#page-load").fadeOut(800);
	}
}

function positionFooter()
{
	setTimeout(function(){
		var windowHeight = jQuery(window).height();
		var footerHeight = jQuery('footer').height();
		var bodyHeight = jQuery('body').height();

		if(bodyHeight < windowHeight)
		{
			jQuery('footer').addClass('stuck');
		}

		jQuery('footer').addClass('visible');
	},500);
}

function stickyHeader()
{
	var headerHeight = jQuery('header').outerHeight();
	var navHeight = jQuery('nav').outerHeight();
	var navOffset = headerHeight - navHeight;

	jQuery(window).scroll(function()
	{
		if(jQuery(this).scrollTop() > navOffset - 19 )
		{
			jQuery('body').addClass('sticky');
			jQuery('#content').css('padding-top',navHeight);
		}
		else
		{
			jQuery('body').removeClass('sticky');
			jQuery('#content').css('padding-top',0);
		}
	});
}
