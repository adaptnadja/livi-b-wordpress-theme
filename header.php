<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LiviB
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php
	if( have_rows( 'scripts', 'options' ) ) :
		while( have_rows( 'scripts', 'options' ) ) :
			the_row();
			the_sub_field( 'script' );
		endwhile;
	endif;
?>
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<div id="page-load">
		<?php
			$load = get_field( 'loading_icon', 'options' );
			if( !empty( $load ) ) : ?>
				<img src="<?php echo $load['url']; ?>"/>
				<?php
			endif;
		?>
	</div>
	<div class="site-container">
			<div id="page" class="site">
				<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'livib' ); ?></a>

				<header id="masthead" class="site-header" role="banner">
					<div class="container">
						<div class="logo">
							<?php
							$image = get_field('logo', 'options');
								if(!empty($image)):?>
								<a href="<?php echo get_option("siteurl"); ?>"><img src="<?php echo $image['url'];?>"></a>
							<?php endif; ?>
						</div>
					</div>
					<nav class="navbar navbar-default">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed hamburger hamburger--elastic" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					        <span class="hamburger-box"></span>
					        <span class="hamburger-inner"></span>
					      </button>
					    </div>
					    <?php
					    	wp_nav_menu(array(
						    	'menu' => 'main-menu',
						    	'container_class' => 'nav collapse navbar-collapse',
						    	'menu_class' => 'nav navbar-nav',
						    	'container_id' => 'navbar-collapse'
					    	));
					    ?>
						</nav>
				</header><!-- #masthead -->
				<div id="content" class="site-content">
					<div class="container">
