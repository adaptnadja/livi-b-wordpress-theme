	<?php
		get_header();
		if(have_posts()):
			while(have_posts()):
				the_post(); ?>
					<h1 class="single-title text-center s-r"><?php the_title(); ?></h1>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center s-r">
							<div class="portfolio-content">
								<?php the_content();?>
							</div>
						</div>
					</div>
					<?php
			endwhile;
		endif;
	?>
	</div> <!-- / Closing container -->
</div> <!-- / Closing content -->

<p id="swipe-message"><i class="fa fa-arrow-left"></i> Swipe <i class="fa fa-arrow-right"></i></p>
<div id="single" class="slick s-r">
	<?php
		$images = get_field('gallery');
			if(!empty($images)):
				foreach($images as $image):?>
					<a href="<?php echo $image['url']; ?>" data-lightbox="portfolio-image">
						<img src="<?php echo $image['url']; ?>"/>
					</a>
				  <?php
			  endforeach;
		  endif;
	  ?>
</div>
<div class="container">
<?php get_footer(); ?>
