<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package LiviB
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<section class="error-404 not-found">
					<div class="row">
						<h1 class="text-center">Oops! Sorry that page cannot be found!<br></h1>
						<div class="home-button">
							<a href="<?php bloginfo( 'url' ); ?>" class="btn"><i class="fa fa-arrow-left"></i>Go home</a>
						</div>
					</div>
				</section><!-- .error-404 -->
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
