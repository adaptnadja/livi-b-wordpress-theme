<?php get_header(); ?>

<div class="grid">
	<?php 
		$posts = get_homepage_posts();
		if(!empty($posts)):
			foreach($posts as $post):
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true); ?>
				<div class="grid-item grid-item--width2 grid-sizer s-r">
					<a href="<?php echo get_permalink($post->ID); ?>">
					  <img src="<?php echo $image[0]; ?>">
					  <div class="post-overlay">
						  <h3><?php echo $post->post_title; ?></h3>
							<i class="fa fa-camera-retro"></i>
					  </div>
					</a>
			  </div>
			  <?php
			endforeach;
		endif;
	?>
</div>
<?php get_footer(); ?>