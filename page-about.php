<?php
/*
Template Name: About
*/

get_header();?>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<h1>A little bit about me...</h1>
		<?php
			if(have_posts()):
				while(have_posts()):
					the_post();
						the_content();
				endwhile;
			endif;
		?>
	</div>
</div>
<?php get_footer();?>