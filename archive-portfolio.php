<?php get_header(); ?>
<div class="grid">
	<?php 
		if(have_posts()):
			while(have_posts()):
				the_post(); ?>
					<div class="grid-item grid-item--width2 grid-sizer s-r">
						<a href="<?php echo get_permalink($post->ID); ?>">
							<?php the_post_thumbnail(); ?>
						  <div class="post-overlay">
							  <h3><?php echo $post->post_title; ?></h3>
								<i class="fa fa-camera-retro"></i>
						  </div>
						</a>
				  </div>
				  <?php
		  endwhile;
	  endif;
  ?>
</div>
<?php get_footer(); ?>