<?php
/*
* Template Name: Contact
*
*/

get_header();?>
<div class="row">
	<h1 class="text-center">Contact me</h1>
	<div class="col-md-6">
		<?php echo do_shortcode('[contact-form-7 id="46" title="Contact page form"]'); ?>
	</div>
	<div class="col-md-4 hidden-xs hidden-sm col-md-offset-1">
		<ul class="contact-details">
			<li>T: <?php the_field('mobile_number', 'options');?></li>
			<li>E: <?php the_field('email_address', 'options');?></li>
		</ul>
		<div class="social-media">
			<a href="<?php the_field('facebook', 'options');?>"><i class="fa fa-facebook"></i></a>
			<!-- <a href="<?php //the_field('twitter', 'options');?>"><i class="fa fa-twitter"></i></a>
			<a href="<?php //the_field('instagram', 'options');?>"><i class="fa fa-instagram"></i></a> -->
		</div>
	</div>
</div>
<?php get_footer();?>
