<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LiviB
 */

?>
			</div>

				<footer id="colophon" class="site-footer" role="contentinfo">
					<div class="container">
						<div class="row">
							<div class="col-sm-4 col-md-4">
								<a id="phone" href="tel:<?php the_field('mobile_number');?>"><?php echo the_field('mobile_number', 'options');?></a>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="social-media">
									<a href="<?php the_field('facebook', 'options');?>" target="_blank"><i class="fa fa-facebook"></i></a>
									<!-- <a href="<?php //the_field('twitter', 'options');?>" target="_blank"><i class="fa fa-twitter"></i></a>
									<a href="<?php //the_field('instagram', 'options');?>" target="_blank"><i class="fa fa-instagram"></i></a> -->
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<a id="email" href="mailto:<?php echo the_field('email_address');?>"><?php echo the_field('email_address', 'options');?></a>
							</div>
						</div>
						<div class="row">
							<div class="bottom">
								<div class="col-sm-6 col-md-6">
									<p id="copyright">&copy; <?php echo date('Y');?> Livi B Photography. All rights reserved.</p>
								</div>
								<div class="col-sm-6 col-md-6">
									<p id="credit">Delivered with <i class="fa fa-heart"></i> by <a href="http://www.adaptdigital.co" target="_blank">Adapt</a></p>
								</div>
							</div>
						</div>
					</div>
				</footer><!-- #colophon -->

			</div><!-- #page -->


		<?php wp_footer(); ?>
		</div>
		</body>
</html>
